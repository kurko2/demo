import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12


Column {
    id: rowsList
    width: mainWindow.width - menu.width
    spacing: 10
    anchors {
       left: menu.right
       top: menu.top
       topMargin: 10
    }

    ListView {
        id: listView
        height: mainWindow.height
        anchors {
            left: parent.left
            right: parent.right
            //top: parent.top
            leftMargin: 10
            rightMargin: 10
            //topMargin: 10
        }

        boundsBehavior: Flickable.StopAtBounds
        model: streamsModel
        spacing: 15
        delegate: CMSStreamViewDelegete { }
    }

    Behavior on opacity {
        PropertyAnimation {
            target: rowsList
            property: "opacity"
            duration: 500
        }
    }

}
