import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Component {
    RowLayout {
        width: parent.width
        height: 40

        Rectangle {
            height: parent.height
            width: channelNumberText.width + 30
            radius: 10
            border.width: 2
            border.color: "gray"
            gradient: Gradient {
                GradientStop { position: 0; color: "antiquewhite" }
                GradientStop { position: 1; color: "black" }
            }

            Text {
                id: channelNumberText
                color: "white"
                anchors.centerIn: parent
                text: channelNumber
            }
        }

        Rectangle {
            height: parent.height
            radius: 10
            border.width: 2
            border.color: "gray"
            Layout.fillWidth: true
            //Layout.margins: -5
            //width: row.width + 20
            gradient: Gradient {
                GradientStop { position: 0; color: "lightsteelblue" }
                GradientStop { position: 1; color: "black" }
            }

            Row {
                anchors.centerIn: parent
                spacing: 10

                Text {
                    color: "white"
                    text: qsTr("Источник: ") + sourceUrl
                    Layout.fillWidth: true
                }

                Text {
                    color: "white"
                    text: qsTr("Последний рестарт: ") + lastRestartTime
                    Layout.fillWidth: true
                }
            }
        }                
    }
}
