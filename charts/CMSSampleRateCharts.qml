import QtQuick 2.0
import QtCharts 2.3

Item {
    id: chart
    width: mainWindow.width - menu.width
    anchors.left: menu.right
    height: mainWindow.height

    ChartView {
        anchors.fill: parent
        legend.alignment: Qt.AlignBottom
        antialiasing: true

        Text {
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.leftMargin: 15
            anchors.bottomMargin: 15
            text: qsTr("Время старта сервиса: ") + Qt.formatDateTime(streamsModel.serviceStartTime, "yyyy-MM-dd hh:mm:ss")
        }

        BarSeries {
            id: mySeries
            axisX:  BarCategoryAxis{categories: [" "]}//ValueAxis { min: -0.5; max: 0.5; }//BarCategoryAxis{ categories: [1010,1020,1030,1040,1050] }
            axisY:  ValueAxis { min: 43950; max: 44150; tickInterval: 1; minorTickCount: 4; } //3

            HBarModelMapper {
                model: streamsModel
                firstBarSetRow: 0
                lastBarSetRow: 5
                columnCount: 1
                firstColumn: 4
            }
        }
    }

    Behavior on opacity {
        PropertyAnimation {
            target: chart
            property: "opacity"
            duration: 500
        }
    }
}
