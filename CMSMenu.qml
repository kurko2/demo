import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Rectangle {
    id: menu
    width: 150
    height: mainWindow.height
    color: "steelblue"
    gradient: Gradient {
        orientation: Gradient.Horizontal
        GradientStop { position: 0; color: "steelblue" }
        GradientStop { position: 1; color: "black" }
    }

    Image {
        id: arrowImage
        opacity: !buttonsColumn.visible
        source: "qrc:/images/images/arrow.png"
        anchors.verticalCenter: parent.verticalCenter
        rotation: -90
    }

    ListModel {
        id: buttonsModel
        // lifehack: https://agateau.com/2018/working-around-listmodel-limitations/
        Component.onCompleted: {[
            {
                name: "Каналы",
                screen: streamsListScreen
            },
            {
                name: "Sample rate",
                screen: chartsSampleRateScreen
            },
            {
                name: "Падения",
                screen: chartsFallScreen
            },
            {
                name: "Изменить",
                screen: undefined
            }].forEach(function(e) { append(e); });
        }
    }

    Column {
        id: buttonsColumn
        anchors.fill: parent

        Repeater {
            model: buttonsModel
            delegate: Button {
                id: manageButton
                width: parent.width-10
                anchors.horizontalCenter: parent.horizontalCenter
                text: name

                onClicked: {
                    menu.state = "noMenu"
                    menuMouseArea.enabled = true

                    currentScreen.opacity = 0.0
                    currentScreen = model.screen
                    currentScreen.opacity = 1.0
                }
            }
        }
    }

    Button {
        id: collapseButton
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        text: "-"

        onClicked: {
            menu.state = "noMenu"
            menuMouseArea.enabled = true
        }
    }

    MouseArea {
        id: menuMouseArea
        enabled: false
        anchors.fill: parent
        onClicked: {
            menu.state = "activeMenu"
            menuMouseArea.enabled = false
        }
    }

    states: [
        State {
            name: "noMenu"
            PropertyChanges { target: menu; width: 15; }
            PropertyChanges { target: buttonsColumn; visible: false; enabled: false; }
            PropertyChanges { target: collapseButton; visible: false; enabled: false; }

        },
        State {
            name: "activeMenu"
            PropertyChanges { target: menu; width: 150; }
            PropertyChanges { target: buttonsColumn; visible: true; enabled: true; }
            PropertyChanges { target: collapseButton; visible: true; enabled: true; }
        }
    ]


    transitions: Transition {
        SequentialAnimation{
            PropertyAnimation { properties: "width"; easing.type: Easing.Linear; duration: 200; }
            PropertyAnimation { properties: "visible"; easing.type: Easing.Linear; duration: 0; }
        }
    }
}
