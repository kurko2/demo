#ifndef NETWORKINTERFACE_H
#define NETWORKINTERFACE_H

#include <QObject>
#include <QWebSocket>
#include "qttricks/qqmlhelpers.h"

class NetworkInterface : public QObject
{
    Q_OBJECT
    SINGLETON(NetworkInterface)
    QML_READONLY_PROPERTY(QString, serviceState)

public:
    explicit NetworkInterface(QObject *parent = nullptr);

signals:

public slots:
    void sendRequest();

private:
    QScopedPointer<QWebSocket> m_socket;

};

#endif // NETWORKINTERFACE_H
