#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include "StreamsListModel.h"

// Количество комплектов
StreamsListModel::StreamsListModel(QObject * parent) : QAbstractTableModel(parent)
{
}

StreamsListModel::~StreamsListModel()
{
    m_rows.clear();
}

int StreamsListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return m_rows.size();
}

QVariant StreamsListModel::data(const QModelIndex &index, int role) const
{
    // Вот такой вот лайфхак специально для Charts
    // А еще можно переопределять index(), а еще можно перейти к QAbstractTableModel от QAbstractListModel
    if (index.column() != 0) {
        role = Qt::UserRole + 1 + index.column();
    }

    if ((!index.isValid())
            || (index.row() >= m_rows.size()))
        return QVariant();

    switch(role)
    {
    case CHANNEL_NUMBER_ROLE:
        return m_rows.at(index.row())->id;
    case NAME_ROLE:
        return m_rows.at(index.row())->name;
    case URL_ROLE:
        return m_rows.at(index.row())->url;
    case FALL_COUNT_ROLE:
        return m_rows.at(index.row())->fallCount;
    case SAMPLERATE_ROLE:
        return m_rows.at(index.row())->sampleRate;
    case LAST_RESTART_TIME_ROLE:
        return m_rows.at(index.row())->lastRestartTime;
    default:
        return QVariant();
    }

    return QVariant();
}

QHash<int, QByteArray> StreamsListModel::roleNames() const
{
    const QHash<int, QByteArray> roles({{CHANNEL_NUMBER_ROLE, "channelNumber"},{NAME_ROLE, "streamName"},{URL_ROLE, "sourceUrl"},{FALL_COUNT_ROLE,"fallCount"},{SAMPLERATE_ROLE,"sampleRate"},{LAST_RESTART_TIME_ROLE,"lastRestartTime"}});
    return roles;
}

int StreamsListModel::columnCount(const QModelIndex &parent) const
{
    return 5;
}

QVariant StreamsListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    return data( createIndex(section, 0), CHANNEL_NUMBER_ROLE );
}

void StreamsListModel::updateModel(const QJsonArray &jsonArray)
{
    qint32 maxFallCount = 0;
    beginResetModel();
    m_rows.clear();
    for (auto i = jsonArray.constBegin(); i != jsonArray.constEnd(); ++i) {
        qint32 fallCount = (*i).toObject().value("FallCount").toString().toInt();
        (fallCount > maxFallCount) ? (maxFallCount = fallCount) : (maxFallCount);

        m_rows.append(
                    QSharedPointer<Stream>(
                        new Stream(
                            (*i).toObject().value("ChannelNumber").toString().toInt(),
                            (*i).toObject().value("Name").toString(),
                            (*i).toObject().value("Url").toString(),
                            fallCount ,
                            (*i).toObject().value("SampleRate").toString().toInt(),
                            (*i).toObject().value("LastRestartTime").toString() )));
    }

    update_maxFallCount(maxFallCount);
    endResetModel();
}
