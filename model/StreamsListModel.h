#ifndef STREAMLISTMODEL_H
#define STREAMLISTMODEL_H

#include <QAbstractListModel>
#include <QDateTime>
#include <QSharedPointer>
#include "qttricks/qqmlhelpers.h"

class StreamsListModel : public QAbstractTableModel
{
    Q_OBJECT
    SINGLETON(StreamsListModel)
    QML_READONLY_PROPERTY(int, maxFallCount)
    QML_READONLY_PROPERTY(QDateTime, serviceStartTime)

public:
    StreamsListModel(QObject *parent);
    virtual ~StreamsListModel();

    // Обязаны быть переопределены
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    int columnCount(const QModelIndex &parent) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;



    void updateModel(const QJsonArray &jsonArray);

    struct Stream
    {
        Stream(const quint32 id, const QString &name, const QString &url, quint32 fallCount, quint32 sampleRate, const QString & lastRestartTime):
            id(id),
            name(name),
            url(url),
            fallCount(fallCount),
            sampleRate(sampleRate),
            lastRestartTime(lastRestartTime)
        {   }

        quint32 id;
        QString name;
        QString url;
        quint32 fallCount;
        quint32 sampleRate;
        QString lastRestartTime;
    };

private:
    enum Column {
        CHANNEL_NUMBER_ROLE = Qt::UserRole + 1,
        NAME_ROLE,
        URL_ROLE,
        FALL_COUNT_ROLE,
        SAMPLERATE_ROLE,
        LAST_RESTART_TIME_ROLE,
        END_OF_COLUMN_ROLE
    };

    QList<QSharedPointer<Stream> > m_rows;

};

#endif // STREAMLISTMODEL_H
