#include <QSslError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "NetworkInterface.h"
#include "model/StreamsListModel.h"

#define DB_DATETIME_FORMAT      "yyyy-MM-dd hh:mm:ss.zzz"

NetworkInterface::NetworkInterface(QObject *parent) : QObject(parent)
{
    m_socket.reset(new QWebSocket());

    connect(m_socket.data(), &QWebSocket::textMessageReceived, [this](const QString & response){

        QJsonParseError error;
        QJsonDocument jsonDoc = QJsonDocument::fromJson(response.toUtf8(), &error);
        QJsonObject rootObj = jsonDoc.object();
        if (error.error != QJsonParseError::NoError)
            qDebug() << "config error: " << error.errorString() << " at " << error.offset;

        update_serviceState(rootObj.value("State").toString());
        StreamsListModel::getInstance()->updateModel(rootObj.value("Streams").toArray());
        StreamsListModel::getInstance()->update_serviceStartTime(QDateTime::fromString( rootObj.value("StartTime").toString(), DB_DATETIME_FORMAT ));

        //qDebug() << rootObj.value("Streams");

        m_socket->close();
    });

    connect(m_socket.data(), &QWebSocket::connected, [&](){
        QJsonDocument jsonDoc;
        QJsonObject rootObject;
        rootObject.insert("Request", "getState");
        jsonDoc.setObject(rootObject);
        m_socket->sendTextMessage(jsonDoc.toJson());
    });

//    connect(m_socket.data(), QOverload<const QList<QSslError>&>::of(&QWebSocket::sslErrors), [this](const QList<QSslError> & errorList){
//        //Q_UNUSED(errorList)
//        m_socket->ignoreSslErrors();
//        qDebug() << errorList
//    });
}

void NetworkInterface::sendRequest()
{
    m_socket->open(QUrl(QStringLiteral("ws://192.168.73.125:8888")));
}
