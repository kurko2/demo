import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "charts"


ApplicationWindow
{
    id: mainWindow

    property Item currentScreen: chartsFallScreen//streamsListScreen

    visible: true
    width: 1000
    height: 480
    title: qsTr("sfpd managing console")

    Timer {
        interval: 1000
        repeat: true
        running: true
        triggeredOnStart: true
        onTriggered: networkInterface.sendRequest()
    }

    CMSMenu {
        id: menu
    }

    CMSStreamsList {
        id: streamsListScreen
        opacity: 0.0
    }

    CMSSampleRateCharts {
        id: chartsSampleRateScreen
        opacity: 0.0
    }

    CMSFallCharts {
        id: chartsFallScreen
    }
}
