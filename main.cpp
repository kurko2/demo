#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "NetworkInterface.h"
#include "model/StreamsListModel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);
//    NetworkInterface * networkInterface = new NetworkInterface(qobject_cast<QObject *>(&app)) ;
//    StreamsListModel * streamsModel = new StreamsListModel(qobject_cast<QObject *>(&app)) ;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("networkInterface", NetworkInterface::getInstance(qobject_cast<QObject *>(&app)));
    engine.rootContext()->setContextProperty("streamsModel", StreamsListModel::getInstance(qobject_cast<QObject *>(&app)));

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
